import { Route } from '@angular/router';
import { RemoteEntryComponent } from './entry.component';

export const remoteRoutes: Route[] = [
  { path: '', redirectTo: 'objcon-list', pathMatch: 'full' },
  {
    path: '',
    component: RemoteEntryComponent,
    children: [
      {
        path: 'objcon-list',
        data: { preload: false },
        loadChildren: () =>
          import('@nx-test/objcon-list').then((mod) => mod.ObjconListModule),
      },
    ],
  },
];
