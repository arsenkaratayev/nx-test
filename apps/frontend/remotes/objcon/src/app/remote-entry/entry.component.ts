import { Component } from '@angular/core';

@Component({
  selector: 'nx-test-objcon-entry',
  template: `
    <div>{{ title }}</div>
    <div><router-outlet></router-outlet></div>
  `,
})
export class RemoteEntryComponent {
  public title = 'objcon';
}
