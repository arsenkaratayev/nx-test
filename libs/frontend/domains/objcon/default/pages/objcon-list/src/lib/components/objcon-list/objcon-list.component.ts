import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'nx-test-objcon-list',
  templateUrl: './objcon-list.component.html',
  styleUrls: ['./objcon-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ObjconListComponent {}
