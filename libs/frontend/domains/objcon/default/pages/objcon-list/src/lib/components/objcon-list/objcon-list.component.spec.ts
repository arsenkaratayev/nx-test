import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ObjconListComponent } from './objcon-list.component';

describe('ObjconListComponent', () => {
  let component: ObjconListComponent;
  let fixture: ComponentFixture<ObjconListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ObjconListComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ObjconListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
