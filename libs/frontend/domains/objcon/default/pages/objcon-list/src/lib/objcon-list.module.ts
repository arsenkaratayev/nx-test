import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ObjconListComponent } from './components/objcon-list/objcon-list.component';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: ObjconListComponent,
        children: [],
      },
    ]),
    CommonModule,
  ],
  declarations: [ObjconListComponent],
})
export class ObjconListModule {}
